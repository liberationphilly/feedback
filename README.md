# Feedback form

This simple web form can be used during an event to ask for feedback.
The presenter can then view the responses by entering the key.

It stores data with Python shelve.

```
mkvirtualenv feedback --python=python3
pip install -r requirements.txt
SECRET_KEY="example pass" python server.py
```

It can be deployed to Dokku.
Set SECRET_KEY as a config variable.
