from flask import Flask, render_template, request
import os
from os import path
import shelve
app = Flask(__name__)
db = shelve.open(path.join(app.root_path, 'shelve.db'), writeback=True)

@app.route("/")
def index():
    return render_template('index.html')

@app.route("/submit", methods=['POST'])
def submit():
    message = request.form['message']

    if message:
        db.setdefault('messages', [])
        db['messages'].append(message)

    return render_template('submitted.html', submitted=True)

@app.route("/view")
def view():
    key = request.args.get('key')

    # Require the key to view responses
    if not key:
        return render_template('login.html')

    # Handle it if the key is wrong
    if key and key != os.environ.get('SECRET_KEY'):
        return render_template('login.html', error=True)

    # The key is correct, show the responses
    db.setdefault('messages', [])
    messages = db['messages']
    return render_template(
        'view.html',
        messages=list(reversed(messages))  # reverse order
    )

if __name__ == "__main__":
    app.run(
        host="0.0.0.0",
        port=5000
    )
